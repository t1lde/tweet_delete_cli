#![deny(warnings)]
extern crate egg_mode;
extern crate tokio;
extern crate chrono;
#[macro_use] extern crate text_io;

extern crate structopt;
use structopt::StructOpt;
use chrono::TimeZone;

use tokio::runtime::current_thread::block_on_all;

fn parse_key(src: &str) -> String  {
   src.trim().to_string()
}

fn parse_datetime(src: &str) -> chrono::DateTime<chrono::Utc> {
    chrono::DateTime::from(chrono::Local.datetime_from_str(src, "%Y/%m/%d %H:%M:%S")
                   .unwrap())
}

fn parse_duration(src: &str) -> chrono::DateTime<chrono::Utc> {
    println!("{}", src.to_string());
    let captures = regex::Regex::new(r"(?x)
                                      ^
                                  \p{White_Space}*
                                  (?:
                                    (?P<months>\d{1,2} )
                                    \p{White_Space}+
                                    (?i: months?)
                                  )?
                                  \p{White_Space}*
                                  (?:
                                    (?P<weeks> \d{1,2} )
                                    \p{White_Space}+
                                    (?i: weeks?)
                                  )?
                                  \p{White_Space}*
                                  (?:
                                    (?P<days>\d{1,3} )
                                    \p{White_Space}+
                                    (?i: days?)
                                  )?
                                  \p{White_Space}*
                                  (?:
                                    (?P<hours> \d{1,4})
                                    \p{White_Space}+
                                    (?i: hours?)
                                  )?
                                  \p{White_Space}*
                                  (?:
                                    (?P<minutes> \d{1,4})
                                    \p{White_Space}+
                                    (?i: minutes?)
                                  )?
                                  \p{White_Space}*
                                  $"
                                  ).expect("Compiling regex").captures(src).expect("Getting captures");

    println!("{:?}", captures);
    let parse_match = |m: regex::Match| m.as_str().parse::<u16>().unwrap();
    let (months, weeks, days, hours, minutes) = (captures.name("months").map_or(0, parse_match),
                                                 captures.name("weeks").map_or(0, parse_match),
                                                 captures.name("days").map_or(0, parse_match),
                                                 captures.name("hours").map_or(0, parse_match),
                                                 captures.name("minutes").map_or(0, parse_match));
    match (months, weeks, days, hours, minutes) {
       (13..=99, ..) => panic!("Months must not exceed a year (12)"),
       (_, 53..=99, ..) => panic!("Weeks must not exceed a year (52)"),
       (_, _, 367..=999, ..) => panic!("Days must not exceed a year (366)"),
       (_, _, _, 8785..=9999, _) => panic!("Hours must not exceed a year (8784)"),
       (1..=12, 5..=52, ..) => panic!("When months > 0, weeks must not exceed a month (4)"),
       (1..=12, 0, 31..=366, ..) => panic!("When months >0, days must not exceed a month (31)"),
       (1..=12, 0, 0, 744..=8784,  _) => panic!("When months >0, hours must not exceed a month (744)"),
       (_, 1..=52, 7..=366, ..) => panic!("When weeks > 0, days must not exceed a week (7)"),
       (_, 1..=52, 0, 168..=8784, _) => panic!("When weeks > 0, hours must not exceed a week (168)"),
       (_, _, 1..=366, 24..=8784, _) => panic!("When days > 0, hours must not exceed a day (24)"),
       (_, _, 1..=366, _, 1440..=9999) => panic!("When days > 0, minutes must not exceed a day (1440)"),
       (_, _, _, 1..=8784, 60..=9999) => panic!("When hours >0, minutes must not exceed an hour (60)"),
       (..) => ()
    }
    (chrono::Utc::now() - chrono::Duration::weeks(i64::from(weeks)) - chrono::Duration::days(i64::from(days + 31*months)) - chrono::Duration::hours(i64::from(hours)) - chrono::Duration::minutes(i64::from(minutes)))
}

#[derive(StructOpt, Debug)]
#[structopt(name = "tweet delete cli")]
struct CliArgs {
    #[structopt(short, long)]
    pin_auth: bool,

    #[structopt(long, parse(from_str="parse_key"))]
    consumer_public: String,

    #[structopt(long, parse(from_str="parse_key"))]
    consumer_private: String,

    #[structopt(long, parse(from_str="parse_key"))]
    access_private: Option<String>,

    #[structopt(long, parse(from_str="parse_key"))]
    access_public: Option<String>,

    #[structopt(long="from", parse(from_str="parse_datetime"))]
    delete_from_date: Option<chrono::DateTime<chrono::Utc>>,

    #[structopt(long="all")]
    delete_all: bool,

    #[structopt(long, parse(from_str="parse_duration"))]
    delete_from_duration: Option<chrono::DateTime<chrono::Utc>>,

    #[structopt(short, long)]
    dry: bool,

    #[structopt(short, long)]
    force: bool,
}



fn get_oauth_pin_access(consumer_public: &str, consumer_private: &str){
    print!("Requesting twitter authorisation...");
    let con_token = egg_mode::KeyPair::new(consumer_public.to_string(), consumer_private.to_string());
    let request_token = block_on_all(egg_mode::request_token(&con_token, "oob")).unwrap();
    let auth_url = egg_mode::authorize_url(&request_token);

    println!("\nGo to {} to recieve auth PIN\nEnter PIN:", auth_url);
    let pin: u32 = read!();
    let (token, _, _) = block_on_all(egg_mode::access_token(con_token, &request_token, pin.to_string())).expect("PIN Auth failed");
    match token {
        egg_mode::Token::Access{access: acc_keypair, ..} => println!("Store these somewhere safe!\nAccess Token:\n\tPublic: {}\n\tPrivate: {}", acc_keypair.key, acc_keypair.secret),
        _ => ()
    }

}


enum PromptType {
    Interactive,
    DryRun,
    Force
}

struct QueryParams {
    auth_token: egg_mode::Token,
    delete_from: chrono::DateTime<chrono::Utc>,
    prompt_type: PromptType,
}

fn query_params (args: &CliArgs) -> QueryParams {
    QueryParams{
        auth_token: egg_mode::Token::Access {
            consumer: egg_mode::KeyPair::new(args.consumer_public.to_string(), args.consumer_private.to_string()),
            access: egg_mode::KeyPair::new(
                args.access_public.as_ref().expect("Required argument access_public not provided.").to_string(),
                args.access_private.as_ref().expect("Required argument access_private not provided.").to_string(),
            ),
        },
        delete_from: match (args.delete_all, args.delete_from_date, args.delete_from_duration) {
            (true,  None,    None   )  => chrono::Utc::now(),
            (false, Some(d), None   )  => d,
            (false, None,    Some(d))  => d,
            (false, None,    None   )  => panic!("Must provide one of delete_all, delete_from_date, delete_from_duration."),
            (..) => panic!("delete_all, delete_from_date, delete_from_duration are mutually exclusive."),
        },
        prompt_type: match (args.dry, args.force) {
            (false, false) => PromptType::Interactive,
            (true,  false) => PromptType::DryRun,
            (false, true ) => PromptType::Force,
            (..) => panic!("dry and force are mutually exclusive.")
        }
    }
}


fn query_tweets (query_params: &QueryParams) {
    println!("Verifying Access Tokens...");
    let auth_token = &query_params.auth_token;
    let user = block_on_all(egg_mode::verify_tokens(auth_token)).expect("Error verifying Access Tokens");
    println!("Authenticated as user: \n\t{}\n\t@{}", user.name, user.screen_name);
    println!("Querying Tweets...");

    let timeline = egg_mode::tweet::user_timeline(user.id, false, false, auth_token).with_page_size(200);
    let (mut timeline, feed) = block_on_all(timeline.start()).unwrap();

    let mut tweets: Vec<egg_mode::tweet::Tweet> = feed.response;
    let mut len = 0;
    while let Some(tweet) = tweets.last() {
        println!("Got {} tweets, Oldest is id:{} {}",tweets.len() ,tweet.id, tweet.created_at);
        if tweet.created_at <= query_params.delete_from {break};
        println!("Loading older tweets...");
        let (tl, feed) = block_on_all(timeline.older(None)).unwrap();
        tweets = feed.response;
        timeline = tl;
        if tweets.len() == len {break};
        len = tweets.len();
    }
    println!("{}", tweets.len());

    let mut tweet_slice: &[egg_mode::tweet::Tweet] = &tweets;
    while tweet_slice.len() > 1 {
        if tweet_slice.first().unwrap().created_at <= query_params.delete_from {break};
        let mid = tweet_slice.len()/2;
        if tweet_slice[mid].created_at > query_params.delete_from  {
            let (_, xs) = tweet_slice.split_at(mid + 1);
            tweet_slice = xs;
            continue;
        };
        let (_, xs) = tweet_slice.split_first().unwrap();
        tweet_slice = xs;
    }

    let tw = tweet_slice.first().expect("No tweets :(");
    let mut len = tweets.len();
    println!("Found newest {} tweets older than query: id:{} {}", tweet_slice.len() ,tw.id, tw.created_at);
    let mut to_delete = tweet_slice.to_vec();
    loop {
        let (tl, feed) = block_on_all(timeline.older(None)).unwrap();
        timeline = tl;
        tweets = feed.response;
        if (tweets.len() == len ) || (tweets.len() == 0 ) {break};
        len = tweets.len();
        to_delete.extend_from_slice(&tweets);
    }

    println!("{} tweets to delete", to_delete.len());
    match query_params.prompt_type {
        PromptType::Interactive => {

            loop {
                println!("Confirm (yes):");
                let x: String = read!("{}");
                if x == "yes" { break };
            }
        },
        PromptType::DryRun => return,
        PromptType::Force => (),
    }

    to_delete.iter().for_each(|tweet| {
        let tw = block_on_all(egg_mode::tweet::delete(tweet.id, auth_token)).unwrap().response;
        println!("Success deleting {}", tw.id);
    });



}

fn main() {
    let args = CliArgs::from_args();
    println!("{:?}", args);

    if args.pin_auth {
        get_oauth_pin_access(&args.consumer_public, &args.consumer_private);
        return;
    }

    query_tweets(&query_params(&args));

}
